import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileFilter;

import capstone.Capstone;

import com.Instruction;
import com.TestListener;
import com.TestManager;
import com.Utils;

public class Main extends JPanel implements ActionListener
{
  private static final long serialVersionUID = 1862563414644241581L;

  static private final String newline = "\n";

  JButton openButton, testButton;

  JTextArea log;

  JFileChooser fc;

  private byte[] loadedBytes;

  private TestManager testManager;

  public Main()
  {
    super(new BorderLayout());

    // Create the log first, because the action listeners
    // need to refer to it.
    log = new JTextArea(5, 20);
    log.setMargin(new Insets(5, 5, 5, 5));
    log.setEditable(false);
    JScrollPane logScrollPane = new JScrollPane(log);

    // Create a file chooser
    String projectHome = System.getProperty("user.dir") + "/testFiles";
    fc = new JFileChooser(projectHome);

    // Uncomment one of the following lines to try a different
    // file selection mode. The first allows just directories
    // to be selected (and, at least in the Java look and feel,
    // shown). The second allows both files and directories
    // to be selected. If you leave these lines commented out,
    // then the default mode (FILES_ONLY) will be used.
    //
    // fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
    fc.setFileHidingEnabled(false);
    fc.setAcceptAllFileFilterUsed(false);
    FileFilter filter = new FileFilter()
    {
      @Override
      public String getDescription()
      {
        return "Exe Only";
      }

      @Override
      public boolean accept(File f)
      {
        if (f.isDirectory())
        {
          return true;
        }

        String extension = Utils.getExtension(f);
        if (extension != null)
        {
          if (extension.equals(Utils.exe))
          {
            return true;
          }
          else
          {
            return false;
          }
        }

        return false;
      }
    };
    fc.addChoosableFileFilter(filter);
    fc.setFileFilter(filter);

    // Create the open button. We use the image from the JLF
    // Graphics Repository (but we extracted it from the jar).
    openButton = new JButton("Load a File...");
    openButton.addActionListener(this);

    testManager = new TestManager();

    // Create the save button. We use the image from the JLF
    // Graphics Repository (but we extracted it from the jar).
    testButton = new JButton("Test a File...");

    testButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        onTestClicked(evt);
      }
    });

    // For layout purposes, put the buttons in a separate panel
    JPanel buttonPanel = new JPanel(); // use FlowLayout
    buttonPanel.add(openButton);
    buttonPanel.add(testButton);

    // Add the buttons and the log to this panel.
    add(buttonPanel, BorderLayout.PAGE_START);
    add(logScrollPane, BorderLayout.CENTER);

  }

  public void onTestClicked(ActionEvent e)
  {
    // testManager.TraceData();
    testManager.Test();
    log.append(testManager.TraceResult());

  }

  static byte[] hexString2Byte(String s)
  {

    int len = s.length();
    byte[] data = new byte[len / 2];
    for (int i = 0; i < len; i += 2)
    {
      data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
    }
    return data;
  }

  public void actionPerformed(ActionEvent e)
  {
    // Handle open button action.
    if (e.getSource() == openButton)
    {
      int returnVal = fc.showOpenDialog(Main.this);

      if (returnVal == JFileChooser.APPROVE_OPTION)
      {
        File file = fc.getSelectedFile();

        try
        {
          loadedBytes = Files.readAllBytes(file.toPath());

////           for test purposes
//          byte[] testBytes = new byte[1024];
//          for (int i = 0; i < 1024; i++)
//          {
//            testBytes[i] = loadedBytes[i];
//          }

          // This is where a real application would open the file.
          log.append("Opening: " + file.getName() + "." + newline);
          log.append("Total Bytes: " + loadedBytes.length + "." + newline);

          Capstone cs = new Capstone(Capstone.CS_ARCH_X86, Capstone.CS_MODE_32 + Capstone.CS_MODE_LITTLE_ENDIAN);
          cs.setSyntax(Capstone.CS_OPT_SYNTAX_INTEL);
          cs.setDetail(Capstone.CS_OPT_ON);
          cs.setSkipData(Capstone.CS_OPT_ON);

          Capstone.CsInsn[] allInsn = cs.disasm(loadedBytes, 0x1000);
          log.append("Instruction Count: " + allInsn.length + "\n");

          testManager.Clear();

          for (int i = 0; i < allInsn.length; i++)
          {

            Instruction ins = new Instruction(allInsn[i].mnemonic, allInsn[i].opStr);
            ins.index = i;
            log.append(ins.toString() + "\n");
            testManager.Add(ins);
          }

          log.append("Disassemble completed.\n");

          cs.close();
        }
        catch (IOException e1)
        {
          e1.printStackTrace();
        }
      }
      else
      {
        log.append("Open command cancelled by user." + newline);
      }
      log.setCaretPosition(log.getDocument().getLength());

      // Handle save button action.
    }
    else if (e.getSource() == testButton)
    {
      int returnVal = fc.showSaveDialog(Main.this);
      if (returnVal == JFileChooser.APPROVE_OPTION)
      {
        File file = fc.getSelectedFile();
        // This is where a real application would save the file.
        log.append("Saving: " + file.getName() + "." + newline);
      }
      else
      {
        log.append("Save command cancelled by user." + newline);
      }
      log.setCaretPosition(log.getDocument().getLength());
    }
  }

  /**
   * Create the GUI and show it. For thread safety, this method should be
   * invoked from the event-dispatching thread.
   */
  private static void createAndShowGUI()
  {

    // Make sure we have nice window decorations.
    JFrame.setDefaultLookAndFeelDecorated(true);
    JDialog.setDefaultLookAndFeelDecorated(true);

    // Create and set up the window.
    JFrame frame = new JFrame("Test");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setPreferredSize(new Dimension(500, 500));

    // Create and set up the content pane.
    JComponent newContentPane = new Main();
    newContentPane.setOpaque(true); // content panes must be opaque
    frame.setContentPane(newContentPane);

    // Display the window.
    frame.pack();
    frame.setVisible(true);
  }

  public static void main(String[] args)
  {
    // Schedule a job for the event-dispatching thread:
    // creating and showing this application's GUI.
    javax.swing.SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        createAndShowGUI();

      }
    });
  }
}
