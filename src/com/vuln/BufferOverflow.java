package com.vuln;

import java.util.ArrayList;

import com.Instruction;
import com.Vulnerability;

public class BufferOverflow extends Vulnerability
{
  public BufferOverflow()
  {
    super("Buffer Overflow", 6);
    Add(new Instruction("imul", "sp"));
    Add(new Instruction("cmp", "byte"));
    Add(new Instruction("add", "byte"));
    Add(new Instruction("insd", "dword"));
    Add(new Instruction("imul", "ebp"));
    Add(new Instruction("bound", "ebp"));
  }

  @Override
  public boolean Check(int index,ArrayList<Instruction> instructionList)
  {
    // TODO Auto-generated method stub
    return true;
  }
}
