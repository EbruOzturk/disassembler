package com.vuln;

import java.util.ArrayList;

import com.Instruction;
import com.Vulnerability;

public class DivideByZero extends Vulnerability
{
  public DivideByZero()
  {
    super("DivideByZero", 2);
    Add(new Instruction("idiv", "dword"));
  }

  @Override
  public boolean Check(int index,ArrayList<Instruction> instructionList)
  {
    Instruction ci = instructionList.get(index);
    int currentIndex = index -1;
    CharSequence memoryAdress = ci.operands;
    while(!instructionList.get(currentIndex).operands.contains(memoryAdress)){
      currentIndex--;
    }
    
    Instruction instruction = instructionList.get(currentIndex);
    
    try
    {
      String value = instruction.operands.split(",")[1].trim();
      if(Integer.parseInt(value) == 0){
        return true;
      }
    }
    catch (Exception e)
    {
      return false;
    }
    return false;
  }

}
