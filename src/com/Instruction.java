package com;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ebru
 */
public class Instruction
{
  public int index;
  public String name;
  public String operands;

  public Instruction(String name, String operands)
  {
    this.name = name;
    this.operands = operands;
  }

  @Override
  public String toString()
  {
    return index + "\t" + name + "\t" + operands;
  }

}
