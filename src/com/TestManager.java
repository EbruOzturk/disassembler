package com;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.vuln.BufferOverflow;
import com.vuln.DivideByZero;
import com.vuln.IntegerOverflow;

/**
 * 
 * @author Ebru
 */
public class TestManager
{

  private ArrayList<Instruction> instructionList;
  public ArrayList<Vulnerability> vulnerabilityList;
  public ArrayList<PossibleVulnerability> possibleVulnerabilityList;

  public TestManager()
  {

    vulnerabilityList = new ArrayList<Vulnerability>();

    vulnerabilityList.add(new BufferOverflow());
    vulnerabilityList.add(new DivideByZero());
    vulnerabilityList.add(new IntegerOverflow());

    CreateTestData();
  }

  final void CreateTestData()
  {
    instructionList = new ArrayList<Instruction>();

    String[] commands = { "mov", "sub", "lea", "push", "jmp", "call", "imul", "and" };

    for (int i = 0; i < 1000; i++)
    {
      Instruction ins = new Instruction(commands[(int) Math.floor(Math.random() * (double) commands.length)], "");
      ins.index = i;
      instructionList.add(ins);
    }
  }

  public void Clear()
  {
    instructionList = new ArrayList<Instruction>();
  }

  public void Add(Instruction ins)
  {
    instructionList.add(ins);
  }

  public void Test()
  {
    if (instructionList == null)
      return;

    possibleVulnerabilityList = new ArrayList<PossibleVulnerability>();

    for (Vulnerability vuln : vulnerabilityList)
    {
      ArrayList<Instruction[]> grams = nGrammmmms(instructionList, vuln.GetCount());
      // System.out.println("Vuln: " + vuln.GetIdentifier());
      for (Instruction[] gram : grams)
      {
        // System.out.println(vuln.GetIdentifier());
        // System.out.println("*******************");
        if (Compare(vuln.GetIdentifier(), gram))
        {
          if (vuln.Check(gram[0].index,instructionList))
          {
            PossibleVulnerability pv = new PossibleVulnerability();
            pv.type = vuln;
            pv.index = gram[0].index;
            pv.instructionList = gram;

            possibleVulnerabilityList.add(pv);
          }
        }
      }
    }
  }

  public boolean Compare(String identifier, Instruction[] grams)
  {
    String gi = "";
    for (Instruction gram : grams)
    {
      if (gram != null)
      {
        gi += gram.name;
      }
    }
    // System.out.println("grams id: " + gi);
    return gi.equals(identifier);
  }

  public String TraceResult()
  {
    String asd = "";
    for (PossibleVulnerability pv : possibleVulnerabilityList)
    {
      asd += "Line " + pv.index + " has " + pv.type.getName() + " error.\n";
    }
    return asd;
  }

  // public String TraceResult2() {
  // String asd = "";
  // for (PossibleVulnerability pv : possibleVulnerabilityList) {
  // asd += "Line " + pv.index + " has " + pv.type.GetIdentifier() +
  // " error.\n";
  // }
  // return asd;
  // }
  public void TraceData()
  {
    FileWriter fw = null;
    try
    {
      fw = new FileWriter("out2hatal�.txt");
    }
    catch (IOException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    for (Instruction ins : instructionList)
    {
      System.out.println("Line " + ins.index + " " + ins.name + " " + ins.operands);
      try
      {
        fw.write("Line " + ins.index + " " + ins.name + " " + ins.operands + "\n");
      }
      catch (IOException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    try
    {
      fw.close();
    }
    catch (IOException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public static ArrayList<Instruction[]> nGrammmmms(ArrayList<Instruction> allInst, int n)
  {
    ArrayList<Instruction[]> grams = new ArrayList<Instruction[]>();
    Instruction[] gram = new Instruction[n];
    for (int j = 0; j < n; j++)
    {
      for (int i = j; i < allInst.size(); i++)
      {
        // System.out.println(i % n);

        gram[(i + j) % n] = allInst.get(i);
        if (((i + j) % n) == n - 1)
        {
          grams.add(gram);
          gram = new Instruction[n];
        }
      }
    }

    grams.add(gram);

    return grams;
  }

}
