package com;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TestListener implements ActionListener {

	TestManager testmanager;
	
	public TestListener(TestManager testmanager) {
		this.testmanager = testmanager;
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	  this.testmanager.TraceData();
		this.testmanager.Test();	
		this.testmanager.TraceResult();
	}

}
